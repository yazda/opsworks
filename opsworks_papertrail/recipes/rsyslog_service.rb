service "rsyslog" do
  restart_command "service rsyslog restart"
  start_command "service rsyslog start"
  supports :restart => true, :reload => true
  action [:enable, :start]
end
